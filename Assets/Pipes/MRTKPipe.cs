﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace Assets.Pipes
{
    [RequireComponent(typeof(MixedRealityLineRenderer))]
    [RequireComponent(typeof(BezierDataProvider))]
    class MRTKPipe : Pipe
    {
        BezierDataProvider dp;

        protected override void Awake()
        {
            var mlr = GetComponent<MixedRealityLineRenderer>();
            dp = GetComponent<BezierDataProvider>();
            mlr.LineDataSource = dp;

            base.Awake();
        }

        protected override void Update()
        {
            
        }

        protected void LateUpdate()
        {
            UpdateLine();
        }

        protected override void MakeLineConnection(Vector3 a, Vector3 b)
        {
            dp.FirstPoint = a;
            dp.LastPoint = b;

            var mid = (a + b) / 2;
            dp.SetPoint(1, new Vector3(mid.x, a.y, mid.z));
            dp.SetPoint(2, new Vector3(mid.x, b.y, mid.z));

            dp.UpdateMatrix();
        }
    }
}
