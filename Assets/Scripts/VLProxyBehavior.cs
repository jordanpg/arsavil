using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Proxy behavior which holds information and allows interaction with "proxy" objects
public class VLProxyBehavior : MonoBehaviour
{
    private HashSet<VLConnection> connections;

    void Awake()
    {
        connections = new HashSet<VLConnection>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Returns true if this proxy connects to an object with the given ID
    public bool IsConnectedObject(string objId)
    {
        foreach(VLConnection connection in connections)
            if(connection.Connects(objId)) return true;
        return false;
    }

    // Add a connection to this proxy
    public bool AddConnection(VLConnection connection)
    {
        return connections.Add(connection);
    }

    // Remove an object from this proxy, removing all related connections.
    // Returns false if this results in this proxy being orphaned, i.e. all connections are removed.
    // Otherwise, returns true.
    public bool DetachObject(string objId)
    {
        // Remove all connections which connect to objId
        connections.RemoveWhere(connection => connection.Connects(objId));

        return connections.Count > 0;
    }
}
