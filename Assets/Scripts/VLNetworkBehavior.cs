﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using Vuforia;

public class VLNetworkBehavior : MonoBehaviour
{
    public float retryPeriod = 1f;
    public bool isP2PServer { get; protected set; } = false;

    private SocketIOComponent comp;
    private VLNetworkObject netObj = null;
    private bool fetchNetObj = false;
    private string p2pConnectAddress = null;

    // private float lastRetry = 0;
    // Start is called before the first frame update
    void Start()
    {
        comp = GetComponent<SocketIOComponent>();
        
        DebugConsole.Log(string.Format("Socket listening @ {0}", comp.url));

        comp.On("open", OnOpen);
        comp.On("debug", DebugMsg);
        comp.On("registerMonitor", RegisterMonitor);
        comp.On("object", NewObject);
        comp.On("objectUpdate", UpdateObject);
        comp.On("objectClose", CloseObject);
        comp.On("confirmP2PServer", ConfirmP2PServer);
        comp.On("monitorCapture", MonitorCapture);
    }

    void Update() {
        /*if(!comp.IsConnected && (Time.time - lastRetry) >= retryPeriod)
        {
            comp.Connect();
            lastRetry = Time.time;
        }*/

        // Grab the our net object from VLMaster
        if(fetchNetObj || (comp.IsConnected && netObj == null))
        {
            if(netObj == null)
            {
                netObj = VLMaster.Instance.GetNetObjFromGameObj(gameObject);
                if(netObj != null)
                {
                    Debug.Log("Found netObj");
                    DebugConsole.Log("Successfully registered netObj");
                    fetchNetObj = false;
                }
                else
                {
                    //Debug.Log("Couldn't find netObj");
                }
            }
            else
            {
                DebugConsole.Log("Adding local net object to master...");
                netObj.socketObj = gameObject;
                VLMaster.Instance.AddNetworkObject(netObj);
                fetchNetObj = false;
            }
        }

        // If we got a P2P connect command before we were ready, do it now if we are.
        if(p2pConnectAddress != null && comp && comp.IsConnected && comp.socket.ReadyState == WebSocketSharp.WebSocketState.Open)
        {
            ConnectP2PServer(p2pConnectAddress);
            p2pConnectAddress = null;
        }
    }
    void OnOpen(SocketIOEvent e)
    {
        Debug.Log("Opened");
        //DebugConsole.Log("Socket opened");
        // For local connections, we need to make a net object ourselves.
        if(netObj == null && comp.url == "ws://127.0.0.1:4567/socket.io/?EIO=4&transport=websocket")
        {
            Debug.Log("Creating net object for local host...");
            netObj = new VLNetworkObject();
            netObj.localIP = "127.0.0.1";
            netObj.port = 4567;
            netObj.id = "default";
            netObj.numDisplays = 1;
        }

        fetchNetObj = true;
    }
    void DebugMsg(SocketIOEvent e)
    {
        DebugConsole.Log(string.Format("{0}", e.data));
    }

    void RegisterMonitor(SocketIOEvent e)
    {
        // Debug.Log("HELP");
        if(netObj == null) return;

        int index = (int)e.data["index"].n;
        int numMonitors = (int)e.data["numMonitors"].n;

        netObj.numDisplays = numMonitors;

        Debug.Log(string.Format("Registering Monitor {0}@{1}", index, netObj.localIP));
        DebugConsole.Log(string.Format("Registering Monitor {0}@{1}", index, netObj.localIP));
        VLMaster.Instance.RegisterDisplay(netObj, (int)index, e.data);
    }

    void NewObject(SocketIOEvent e)
    {
        JSONObject o = e.data;
        o["ip"].str = netObj.localIP;
        DebugConsole.Log(string.Format("New object {0}@{5} ({1},{2}) ({3},{4})", o["id"].str, o["x"].n, o["y"].n, o["width"].n, o["height"].n, VLMaster.GetDisplayIdentifier((int)o["disp"].n, o["ip"].str)));
        VLMaster.Instance.AddObjectTracker(o);
    }

    void UpdateObject(SocketIOEvent e)
    {
        JSONObject o = e.data;
        o["ip"].str = netObj.localIP;
        VLMaster.Instance.UpdateObjectTracker(o);
        // DebugConsole.Log(string.Format("Update object {0}@{5} ({1},{2}) ({3},{4})", o["id"].str, o["x"].n, o["y"].n, o["width"].n, o["height"].n, (int)o["disp"].n));
    }

    void CloseObject(SocketIOEvent e)
    {
        VLMaster.Instance.RemoveObjectTracker(e.data["id"].str);
        DebugConsole.Log(string.Format("Closing object {0}", e.data["id"].str));
    }

    void ConfirmP2PServer(SocketIOEvent e)
    {
        this.isP2PServer = true;
        DebugConsole.Log(string.Format("P2P server successfully started at {0}", comp.url));
    }

    public void StartP2PServer()
    {
        comp.Emit("startP2PServer");
        DebugConsole.Log(string.Format("Requesting P2P server start at {0}", comp.url));
    }
    public void MonitorCapture(SocketIOEvent e)
    {
        var data = e.data;
        var identifier = VLMaster.GetDisplayIdentifier((int)e.data["id"].n, netObj.localIP);

        // Cut off the metadata
        string b64Image = data["img"].str;
        b64Image = b64Image.Split(',')[1];

        VLMaster.Instance.RebuildDisplayTracker(identifier, b64Image);
    }

    public void ConnectP2PServer(string address)
    {
        // If we aren't ready yet, set a flag and try again next update.
        if(!comp || !comp.IsConnected || comp.socket.ReadyState != WebSocketSharp.WebSocketState.Open)
        {
            p2pConnectAddress = address;
            return;
        }

        var dict = new Dictionary<string, string>();
        dict.Add("url", address);
        var data = new JSONObject(dict);
        comp.Emit("connectP2P", data);
        DebugConsole.Log(string.Format("Connecting {0} to P2P server at {1}", comp.url, address));
    }
}
