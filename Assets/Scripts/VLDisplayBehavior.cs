using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

/*
    VLDisplayBehavior handles the tracking and registration of displays.
    It contains functionality to register its GameObject as a display and
    update its image target over the associated network connection.
*/
public class VLDisplayBehavior : DefaultObserverEventHandler
{
    public VLNetworkObject NetworkObject { get; protected set; }
    public int DisplayID { get; protected set; }
    public DisplayInfo Info { get; protected set; }

    public Vector2 targetSize
    {
        get
        {
            var itb = GetComponent<ImageTargetBehaviour>();
            if (!itb) return new Vector2(1f, 1f);
            return itb.GetSize();
        }
    }

    public TargetStatus status
    {
        get
        {
            return mObserverBehaviour.TargetStatus;
        }
    }

    public int displayIdDebug = -1;

    public string targetReplaceDebug;
    public bool targetReplaceSwitch = false;

    //private DataSet ourDataSet;
    public Texture2D nt;
    public bool debugSwap = false;

    public enum ScalingMode
    {
        None,
        HoloLens,
        Android
    }
    public ScalingMode scaleMode = ScalingMode.HoloLens;

    VLDisplayBehavior() : base()
    {
    }

    protected override void Start()
    {
        base.Start();

        Debug.Log(string.Format("{0} {1}", name, targetSize));

        var itb = GetComponent<ImageTargetBehaviour>();
        //Debug.Log(itb.GetSize());

        //OnTargetFound.AddListener(RegisterDisplay);

        mObserverBehaviour.OnTargetStatusChanged += OnTrackableStatusChanged;
    }

    protected void Update()
    {
        if(targetReplaceSwitch)
        {
            SwitchTarget(GUIUtility.systemCopyBuffer);
            targetReplaceSwitch = false;
        }
    }

    private void OnGUI()
    {
        if(debugSwap && nt)
        {
            GUI.DrawTexture(new Rect(10, 10, 100, 100 * (nt.height / nt.width)), nt);
        }    
    }

    protected void OnTrackableStatusChanged(ObserverBehaviour observerBehaviour, TargetStatus status)
    {
        // If the display is being actively tracked, it is within view
        if (status.Status == Status.TRACKED)
        {
            if (NetworkObject == null) RegisterDisplay();
            VLMaster.Instance.SetDisplayInView(gameObject);
        }
        else // Otherwise, it must be undetected or outside the view
        {
            VLMaster.Instance.SetDisplayOutsideView(gameObject);
        }
    }

    // Copy properties from other VLDisplayBehavior, used when target is rebuilt
    public void InitializeFromOther(VLDisplayBehavior other)
    {
        NetworkObject = other.NetworkObject;
        displayIdDebug = DisplayID = other.DisplayID;
        Info = other.Info;

        var sock = NetworkObject.socketObj.GetComponent<SocketIO.SocketIOComponent>();
        sock.On("open", OnOpen);
        //OnTargetFound.RemoveListener(RegisterDisplay);

        DebugConsole.Log(string.Format("Rebuilt display {0} {1}@{2}", gameObject.name, DisplayID, NetworkObject.localIP));
    }

    protected void RegisterDisplay()
    {
        if(displayIdDebug > -1 || VLMaster.Instance.currDisplayNetObj == null) return;

        NetworkObject = VLMaster.Instance.currDisplayNetObj;
        displayIdDebug = DisplayID = VLMaster.Instance.currDisplayInfo.index;
        Info = VLMaster.Instance.currDisplayInfo;

        VLMaster.Instance.SetDisplay(DisplayID, NetworkObject.localIP, gameObject);

        SocketIO.SocketIOComponent sock = NetworkObject.socketObj.GetComponent<SocketIO.SocketIOComponent>();
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["id"] = DisplayID.ToString();
        sock.Emit("monitorRegistered", new JSONObject(data));
        //sock.On("monitorCapture", MonitorUpdate);
        sock.On("open", OnOpen);

        DebugConsole.Log(string.Format("Found display {0} {1}@{2}", gameObject.name, DisplayID, NetworkObject.localIP));
    }

    /*public void SwitchTargetOld(string b64Image)
    {
        Debug.Log("Swapping");
        // Create new Texture2D for the new image
        byte[] img = System.Convert.FromBase64String(b64Image);
        Texture2D newTex = nt = new Texture2D(1,1);
        newTex.LoadImage(img);

        Debug.Log("Tex Made");
        // Pass our image to the RuntimeImageSource    
        ObjectTracker ot = TrackerManager.Instance.GetTracker<ObjectTracker>();
        ot.RuntimeImageSource.SetImage(newTex, Info.physicalWidth, mTrackableBehaviour.Trackable.Name);
        Debug.Log("Image Set");
        Debug.Log(string.Format("ID: {0}, Width: {1}, Name: {2}", Info.index, Info.physicalWidth, mTrackableBehaviour.Trackable.Name));
        // Remove the old Trackable and create a new one using this object
        Trackable t = mTrackableBehaviour.Trackable;
        if(ourDataSet == null)
            ourDataSet = ot.CreateDataSet();
        else
        {
            try
            {
                ot.DeactivateDataSet(ourDataSet);
                ourDataSet.Destroy(t, false);
            }
            catch
            {
                ourDataSet = ot.CreateDataSet();
            }
        }
        TrackerManager.Instance.GetStateManager().DestroyTrackableBehavioursForTrackable(t,false);
        mTrackableBehaviour = ourDataSet.CreateTrackable(ot.RuntimeImageSource, gameObject);
        mTrackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStatusChanged);
        ot.ActivateDataSet(ourDataSet);

        // Correct trackable size since Vuforia seems to try to (incorrectly) fix the size with some magic I do not understand
        var scale = 1f;
        switch(scaleMode)
        {
            case ScalingMode.None:
                scale = 1f;
                break;
            case ScalingMode.HoloLens:
                scale = 1f / Info.physicalWidth;
                break;
            case ScalingMode.Android:
                scale = Info.physicalWidth;
                break;
        }
        transform.localScale = new Vector3(scale, scale, scale);
        Debug.Log("Done?");
    }*/

    public void SwitchTarget(string b64Image)
    {
        Debug.Log("Swapping");
        // Create new Texture2D for the new image
        byte[] img = System.Convert.FromBase64String(b64Image);
        Texture2D newTex = nt = new Texture2D(1, 1);
        newTex.LoadImage(img);

        Debug.Log("Tex Made");
        Debug.Log("Image Set");
        Debug.Log(string.Format("ID: {0}, Width: {1}, Name: {2}", Info.index, Info.physicalWidth, mObserverBehaviour.TargetName));
        // Remove the old Trackable and create a new one using this object
        var imageTarget = VuforiaBehaviour.Instance.ObserverFactory.CreateImageTarget(newTex, Info.physicalWidth, mObserverBehaviour.TargetName);

        // Correct trackable size since Vuforia seems to try to (incorrectly) fix the size with some magic I do not understand
        var scale = 1f;
        switch (scaleMode)
        {
            case ScalingMode.None:
                scale = 1f;
                break;
            case ScalingMode.HoloLens:
                scale = 1f / Info.physicalWidth;
                break;
            case ScalingMode.Android:
                scale = Info.physicalWidth;
                break;
        }
        transform.localScale = new Vector3(scale, scale, scale);
        Debug.Log("Done?");
    }

    public void MonitorUpdate(SocketIO.SocketIOEvent e)
    {
        return;

        JSONObject data = e.data;
        if((int)data["id"].n != DisplayID) return;

        // Don't bother switching if we're being tracked, this will actually make us lose tracking!
        if(mObserverBehaviour.TargetStatus.Status == Status.TRACKED) return;

        // Cut off the metadata
        string str = data["img"].str;
        str = str.Split(',')[1];

        SwitchTarget(str);
    }

    public void OnOpen(SocketIO.SocketIOEvent e)
    {
        // Let the app know we're here in case we had to reconnect
        SocketIO.SocketIOComponent sock = NetworkObject.socketObj.GetComponent<SocketIO.SocketIOComponent>();
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["id"] = DisplayID.ToString();
        sock.Emit("monitorRegistered", new JSONObject(data));
    }
}

