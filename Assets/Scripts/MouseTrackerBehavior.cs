﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTrackerBehavior : MonoBehaviour
{
	public Vector3 BottomLeftCorner = new Vector3(0, 0, 0);
	public Vector3 TopRightCorner = new Vector3(0, 0, 0);

	// Update is called once per frame
	void Update()
    {
		float normMouseX = Input.mousePosition.x / Screen.width;
		float normMouseY = Input.mousePosition.y / Screen.height;

		Vector3 newPos = new Vector3(normMouseX * (TopRightCorner.x - BottomLeftCorner.x), 0, normMouseY * (TopRightCorner.z - BottomLeftCorner.z));

		transform.localPosition = BottomLeftCorner + newPos;
	}
}
