﻿using System;
using UnityEngine;

[Serializable]
public class VLNetworkObject
{
	public string id;
	public int port;
	public int p2pPort;
	public bool p2pServerUp;
	public bool p2pClientUp;
	public string localIP;
	public int numDisplays;
	public GameObject socketObj;
}
