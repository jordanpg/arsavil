﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
/*
    VLMaster is a singleton class that governs high-level interactions
    between different parts of the visual link system.
    In particular, it facilitates cooperation between components for
    networked actions such as display registration and screen object tracking.
*/
public class VLMaster : Singleton<VLMaster>
{
    // If true, this will redirect stdout to Debug
    // This is useful for seeing debug messages from non-Unity libraries.
    // (mostly websocket-sharp in this case)
    public bool redirectStdOut = false;
    public GameObject objectPrefab; // Prefab for screen object trackers
    public VLProxyBehavior proxyPrefab; // Prefab for proxy objects
    public Text displayNumTracker;
    public bool noSwitchOnExtendedTracking = false;

    private List<VLNetworkObject> netObjs; // All network objects
    private List<VLProxyBehavior> proxies; // All proxy objects
    // private Dictionary<int, GameObject> displays; // All displays
    private Dictionary<string, GameObject> displaysIP;
    private Dictionary<string, GameObject> objects; // All screen objects
    private Dictionary<string, JSONObject> objectsMeta; // Screen objects' JSONObjects as sent by servers
    private Dictionary<string, List<string>> relations; // Object relationships
    private Dictionary<string, List<string>> children; // Parent-child relationships

    private HashSet<GameObject> displaysInView; // Set of displays currently in view

    // These are used when registering displays to allow the new display's
    // properties to be globally known.
    public VLNetworkObject currDisplayNetObj { get; protected set; }
    public int currDisplayIndex { get; protected set; }
    public DisplayInfo currDisplayInfo { get; protected set; }

    // Used to keep track of which device is acting as the P2P server host
    public VLNetworkObject p2pServer { get; protected set; }


    public int numDisplaysInView
    {
        get
        {
            return displaysInView.Count;
        }
    }

    private PipeSystem pipesys;

    // Prevent use of this constructor outside
    protected VLMaster()
    {
        netObjs = new List<VLNetworkObject>();
        proxies = new List<VLProxyBehavior>();
        // this.displays = new Dictionary<int, GameObject>();
        displaysIP = new Dictionary<string, GameObject>();
        objects = new Dictionary<string, GameObject>();
        objectsMeta = new Dictionary<string, JSONObject>();
        relations = new Dictionary<string, List<string>>();
        children = new Dictionary<string, List<string>>();
        displaysInView = new HashSet<GameObject>();

        currDisplayNetObj = null;
        currDisplayIndex = -1;

        p2pServer = null;

        if(redirectStdOut)
            UnitySystemConsoleRedirector.Redirect();
    }

    public static string GetDisplayIdentifier(int id, string ip)
    {
        return id.ToString() + "@" + ip;
    }

    public void Start()
    {
        pipesys = GetComponent<PipeSystem>();
        // VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }

    public void OnGUI()
    {
        if (displayNumTracker)
            displayNumTracker.text = string.Format("Displays In View:\n{0}", numDisplaysInView);
    }

    public void OnVuforiaStarted()
    {
        int targetFps = VuforiaBehaviour.Instance.CameraDevice.GetRecommendedFPS();
        if (Application.targetFrameRate != targetFps) Application.targetFrameRate = targetFps;
    }

    // Marks a display as in view
    public void SetDisplayInView(GameObject disp)
    {
        displaysInView.Add(disp);
    }

    // Marks a display as out of view
    public void SetDisplayOutsideView(GameObject disp)
    {
        displaysInView.Remove(disp);
    }

    // Find a proxy which references a particular object ID, or null if none is found
    public VLProxyBehavior FindFirstConnectedProxy(string objId)
    {
        return proxies.Find(proxy => proxy.IsConnectedObject(objId));
    }

    public List<VLProxyBehavior> FindAllConnectedProxies(string objId)
    {
        return proxies.FindAll(proxy => proxy.IsConnectedObject(objId));
    }

    public void RegisterDisplay(VLNetworkObject netObj, int dispIndex, JSONObject obj)
    {
        currDisplayIndex = dispIndex;
        currDisplayNetObj = netObj;
        currDisplayInfo = new DisplayInfo(obj);

        if(dispIndex >= netObj.numDisplays)
            Debug.LogWarning(string.Format("Network object only reports {0} displays, but attempting to register display #{1}.\nSomething is probably wrong.", netObj.numDisplays, dispIndex + 1));
    }

    public void AddNetworkObject(VLNetworkObject netObj)
    {
        if(!netObjs.Contains(netObj)) netObjs.Add(netObj);

        if (this.p2pServer != null) ConnectP2PNetwork(netObj); // Direct device to connect to P2P network
        else if (netObjs.Count > 1) BuildP2PNetwork(); // No need to build P2P network until we have more than one device
    }

    // Rebuild image target for a display
    public void RebuildDisplayTracker(string identifier, string b64Image)
    {
        if (!displaysIP.ContainsKey(identifier))
        {
            Debug.LogWarning(string.Format("Got request to rebuild tracker for {0}, but no display with this name was found!", identifier));
            return;
        }

        var currObj = displaysIP[identifier];
        var currDisp = currObj.GetComponent<VLDisplayBehavior>();

        // Do not switch targets if we are tracking the display
        if (currDisp.status.Status == Status.TRACKED || (noSwitchOnExtendedTracking && currDisp.status.Status == Status.EXTENDED_TRACKED)) return;

        Debug.Log(string.Format("Creating new target for {0}", identifier));
        // Create new Texture2D for the new image
        byte[] img = System.Convert.FromBase64String(b64Image);
        Texture2D newTex = new Texture2D(1, 1);
        newTex.LoadImage(img);

        // Create new ImageTarget
        Debug.Log(string.Format("ID: {0}, Width: {1}, Name: {2}", currDisp.Info.index, currDisp.Info.physicalWidth, currObj.name));
        var imageTarget = VuforiaBehaviour.Instance.ObserverFactory.CreateImageTarget(newTex, currDisp.Info.physicalWidth, currObj.name);
        var newDisp = imageTarget.gameObject.AddComponent<VLDisplayBehavior>();

        // Transfer properties and children to new target
        newDisp.InitializeFromOther(currDisp);
        while (currObj.transform.childCount > 0) currObj.transform.GetChild(0).parent = newDisp.transform;
        displaysIP[identifier] = newDisp.gameObject;
        objects[identifier] = newDisp.gameObject;

        Debug.Log(displaysIP[identifier]); Debug.Log(objects[identifier]);

        // Force update of display's children & display tracker
        UpdateObjectTracker(objectsMeta["DISP" + identifier]);
        foreach (string id in children[identifier]) if(objectsMeta.ContainsKey(id)) UpdateObjectTracker(objectsMeta[id]);

        // Clean up old object
        displaysInView.Remove(currObj);
        Destroy(currObj);
    }

    public void SetDisplay(int dispIndex, string ip, GameObject obj)
    {
        // displays[dispIndex] = obj;
        VLDisplayBehavior disp = obj.GetComponent<VLDisplayBehavior>();
        string identifier = GetDisplayIdentifier(dispIndex, ip);
        displaysIP[identifier] = obj;
        children[identifier] = new List<string>();
        objects[identifier] = obj;
        currDisplayIndex = -1;
        currDisplayNetObj = null;

        // Create metadata for display tracker
        var trackerInfo = new Dictionary<string, JSONObject>
        {
            { "relations", new JSONObject { list = new List<JSONObject>() } }
        };

        // Construct tracker object for this display
        var trackerDict = new Dictionary<string, JSONObject>
        {
            { "x",        new JSONObject(0f) },
            { "y",        new JSONObject(0f) },
            { "width",    new JSONObject(disp.Info.width) },
            { "height",   new JSONObject(disp.Info.height) },
            { "id",       new JSONObject { str = "DISP" + identifier } },
            { "class",    new JSONObject { str = "display" } },
            { "parent",   new JSONObject { str = "" } },
            { "disp",     new JSONObject(dispIndex) },
            { "ip",       new JSONObject { str = ip } },
            { "obj",      new JSONObject(trackerInfo) }
        };

        Debug.Log(trackerDict.ToString());

        AddObjectTracker(new JSONObject(trackerDict));
    }

    public VLNetworkObject GetNetObjFromGameObj(GameObject gameObj)
    {
        foreach(VLNetworkObject i in netObjs)
        {
            if(i.socketObj == gameObj) return i;
        }

        return null;
    }

    public GameObject AddObjectTracker(JSONObject obj)
    {
        if (objects.ContainsKey(obj["id"].str))
        {
            Debug.LogWarning(string.Format("Object {0} already added, skipping...", obj["id"].str));
            return null;
        }

        string identifier = GetDisplayIdentifier((int)obj["disp"].n, obj["ip"].str);

        if (!displaysIP.ContainsKey(identifier))
        {
            Debug.LogWarning(string.Format("Display {0} not known, skipping registration of object {1}...", identifier, obj["id"].str));
            return null;
        }
        DebugConsole.Log(string.Format("Object {0} on display {1}", obj["id"].str, identifier));

        // Find the display object associated with this object
        GameObject dispObj = displaysIP[identifier];
        // Create tracker as child of display
        GameObject newObj = Instantiate(objectPrefab, dispObj.transform);
        newObj.name = obj["id"].str;

        // Add to display's list of children
        children[identifier].Add(obj["id"].str);

        // Add list of children
        children.Add(obj["id"].str, new List<string>());

        objects[obj["id"].str] = newObj;

        // Save object relations
        JSONObject objInfo = obj["obj"];
        List<string> l = relations[obj["id"].str] = new List<string>();
        foreach (var item in objInfo["relations"].list)
            l.Add(item.str);

        // Add object to parent if it exists
        if(children.ContainsKey(obj["parent"].str))
            children[obj["parent"].str].Add(obj["id"].str);

        UpdateObjectTracker(obj);

        return newObj;
    }

    public void UpdateObjectTracker(JSONObject obj)
    {
        string id = obj["id"].str;
        if(!objects.ContainsKey(id)) return;

        Debug.Log(string.Format("Updating {0}", id));
        Debug.Log(obj.ToString());

        // Add updated JSONObject to objectsMeta
        objectsMeta[id] = obj;

        string identifier = GetDisplayIdentifier((int)obj["disp"].n, obj["ip"].str);
        if(!displaysIP.ContainsKey(identifier)) return;

        GameObject tracker = objects[id];
        GameObject dispObj = displaysIP[identifier];

        // Update relations with new info
        JSONObject objInfo = obj["obj"];
        relations[id] = new List<string>();
        foreach (var item in objInfo["relations"].list)
            relations[id].Add(item.str);

        // Update tracker's parent if they moved to a different display
        if(dispObj != tracker.transform.parent.gameObject)
            tracker.transform.parent = dispObj.transform;

        VLDisplayBehavior dispB = dispObj.GetComponent<VLDisplayBehavior>();
        DisplayInfo dispInfo = dispB.Info;
        var itSize = dispB.targetSize;
        //Bounds dispBounds = dispB.bounds;
        
        float w = obj["width"].n;
        float h = obj["height"].n;
        // Adjust positions to represent center of object in screen space
        float x = obj["x"].n + w / 2;
        float y = obj["y"].n + h / 2;
        // Add parent's screen position if applicable
        // TODO: find a solution that allows multiple nesting.
        if(objectsMeta.ContainsKey(obj["parent"].str))
        {
            x += objectsMeta[obj["parent"].str]["x"].n;
            y += objectsMeta[obj["parent"].str]["y"].n;
        }
        // Normalize screen coordinates
        float nx = x / dispInfo.width;
        float ny = y / dispInfo.height;
        float nw = w / dispInfo.width;
        float nh = h / dispInfo.height;
        // Calculate AR coordinates (Y must be flipped)
        float px = (itSize.x / -2) + itSize.x * nx;
        float py = (itSize.y /  2) - itSize.y * ny;

        // Move and resize tracker
        tracker.transform.localPosition = new Vector3(px, 0, py);
        tracker.transform.localScale = new Vector3(itSize.x * nw, 0.0f, itSize.y * nh);
        tracker.transform.localRotation = Quaternion.identity;

        Debug.Log(tracker.transform.localPosition.ToString());
        Debug.Log(tracker.transform.localScale.ToString());

        // Trigger updates for all children of this object
        foreach (string child in children[id])
            if (objectsMeta.ContainsKey(child)) UpdateObjectTracker(objectsMeta[child]);

        EstablishConnections();
    }

    public void RemoveObjectTracker(string id)
    {
        if(!objects.ContainsKey(id)) return;

        DebugConsole.Log(string.Format("Removing object {0}", id));

        // Remove this object from all proxies, and remove any orphaned proxies
        var ourProxies = FindAllConnectedProxies(id);
        Debug.Log(string.Format("{0} connected proxies...", ourProxies.Count));
        foreach(VLProxyBehavior proxy in ourProxies)
        {
            var orphaned = !proxy.DetachObject(id);

            if(orphaned)
            {
                Debug.Log("Destroying orphaned proxy");
                proxies.Remove(proxy);
                Destroy(proxy.gameObject);
            }
        }

        Destroy(objects[id]);
        objects.Remove(id);

        // Remove from display's list of children
        string identifier = GetDisplayIdentifier((int)objectsMeta[id]["disp"].n, objectsMeta[id]["ip"].str);
        children[identifier].Remove(id);

        objectsMeta.Remove(id);
        children.Remove(id);
    }

    // Create links between screen objects, creating any necessary proxies
    // TODO: determine how to handle connecting two objects with pre-existing proxies
    protected void EstablishConnections()
    {
        VLProxyBehavior proxy;

        foreach (var item in objects)
        {
            if(!relations.ContainsKey(item.Key)) continue;

            // Try and find a proxy connected to this object
            proxy = FindFirstConnectedProxy(item.Key);

            foreach (var o in relations[item.Key])
            {
                if(!objects.ContainsKey(o)) continue;

                var transformA = item.Value.transform;
                var transformB = objects[o].transform;

                // If no proxy was found connected to object A, try and find one connected to object B
                if (!proxy)
                {
                    proxy = FindFirstConnectedProxy(o);

                    // If still no proxy is found, then make one
                    if(!proxy)
                    {
                        var midpoint = Vector3.Lerp(transformA.position, transformB.position, 0.5f);
                        var rot = Quaternion.Lerp(transformA.rotation, transformB.rotation, 0.5f);
                        proxy = Instantiate(proxyPrefab, midpoint, rot);

                        proxies.Add(proxy);
                    }
                }

                // Add this connection to the proxy
                proxy.AddConnection(new VLConnection(item.Key, o));

                // Create connections on both sides of the proxy
                pipesys.Connect(transformA, proxy.transform);
                pipesys.Connect(proxy.transform, transformB);
            }

            // Reset proxy for next item
            proxy = null;
        }
    }

    protected void BuildP2PNetwork()
    {
        if(this.netObjs.Count < 1)
        {
            Debug.LogWarning("Cannot build P2P network with no connected devices");
            return;
        }

        // TODO: Handle failures to start P2P server; move on to next net object?
        // Select first connected netObj as P2P host
        var p2pHostObj = this.netObjs[0];
        var p2pHost = p2pHostObj.socketObj.GetComponent<VLNetworkBehavior>();
        p2pHost.StartP2PServer(); // Request device to start P2P server
        p2pHostObj.p2pServerUp = true;
        p2pHostObj.p2pClientUp = true;
        this.p2pServer = p2pHostObj;

        // TODO: Handle P2P server migration
        // Connect all unconnected clients to this server
        this.netObjs.FindAll(o => !o.p2pClientUp).ForEach(o => ConnectP2PNetwork(o));
    }

    protected void ConnectP2PNetwork(VLNetworkObject netObj)
    {
        if (this.p2pServer == null)
        {
            Debug.LogWarning("Cannot connect to P2P network because no server has been created.");
            return;
        }

        // Don't bother reconnecting clients that are marked as connected
        if (netObj.p2pClientUp) return;

        var netComp = netObj.socketObj.GetComponent<VLNetworkBehavior>();
        var p2pAddr = string.Format("{0}:{1}", this.p2pServer.localIP, this.p2pServer.p2pPort);

        // TODO: Handle connection failure
        netComp.ConnectP2PServer(p2pAddr);
        netObj.p2pClientUp = true;
    }
}
