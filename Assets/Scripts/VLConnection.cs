﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Describes a connection between two objects
public struct VLConnection : IEquatable<VLConnection>
{
    public readonly string a;   // First object in connection
    public readonly string b;   // Second object in connection

    public VLConnection(string a, string b)
    {
        this.a = a;
        this.b = b;
    }

    public bool Connects(string id)
    {
        return a == id || b == id;
    }

    public override bool Equals(object obj)
    {
        if ((obj == null) || !GetType().Equals(obj.GetType()))
            return false;
        else
            return Equals((VLConnection)obj);
    }

    public bool Equals(VLConnection other)
    {
        // Connections are equal if they connect the same IDs
        return other.Connects(a) && other.Connects(b);
    }

    public override int GetHashCode()
    {
        return (a.GetHashCode() << 2) ^ b.GetHashCode();
    }

    public static bool operator == (VLConnection connA, VLConnection connB)
    {
        return connA.Equals(connB);
    }

    public static bool operator != (VLConnection connA, VLConnection connB)
    {
        return !connA.Equals(connB);
    }
}
